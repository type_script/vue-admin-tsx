const koa = require('koa')
const bodyParser = require('koa-bodyparser');
const login = require('./api/login')
const theme = require('./api/theme')
const table = require('./api/table')

const app = new koa()
const api = {
    ...login,
    ...theme,
    ...table
}
app.use(bodyParser())
app.use(async ctx => {
    await api[ctx.url.slice(9)](ctx)
})
app.listen(3000, () => {
    console.log('http://localhost:3000')
})
