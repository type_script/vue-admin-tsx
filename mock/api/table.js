const { Random } = require('mockjs')
module.exports = {
    'tablelist': (ctx) => {
        const data = new Array(20).fill(1).map((_,index) => { return {index:index, city: Random.province(), date: Random.date(), name: Random.name(), content: Random.title(), key: Random.id(), tag: new Array(1).fill(Random.color()) } })
        ctx.body = data
    }
}