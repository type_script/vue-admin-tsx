# VUE ADMIN TSX
<a href="https://type_script.gitee.io/vue-admin-tsx">
项目地址:VUE ADMIN TSX
</a>
<p align="center">
  <a href="https://github.com/vuejs/vue">
    <img src="https://img.shields.io/badge/vue-3.0.0-brightgreen.svg" alt="vue">
  </a>
   <a href="https://2x.antdv.com/docs/vue/introduce-cn/">
    <img src="https://img.shields.io/badge/Ant%20Design%20Vue-2.0.0-brightgreen" alt="vue">
  </a>
   <a href="https://github.com/vuejs/jsx-next">
    <img src="https://img.shields.io/badge/vue%20jsx-1.0.0-blue" alt="vue">
  </a>
    <a href="https://www.tslang.cn/">
    <img src="https://img.shields.io/badge/typescript-3.1.0-blue" alt="vue">
  </a>
</p>

## 目录结构
下面是整个项目的目录结构
```
See [Configuration Reference](https://cli.vuejs.org/config/).

├── mock                     # node mock配置
├── public
│   └── logo.png             # LOGO
|   └── index.html           # Vue 入口模板
├── src
│   ├── types                # 全局接口
│   ├── api                  # Api ajax 等
│   ├── assets               # 本地静态资源
│   ├── plugin               # 项目基础配置，插件，全局设置
│   ├── components           # 业务通用组件
│   ├── hooks                # 通用use函数
│   ├── router               # Vue-Router 
│   ├── store                # Vuex
│   ├── locales              # 国际化资源
│   ├── views                # 业务页面入口和常用模板
│   ├── App.vue              # Vue 模板入口
│   └── main.js              # Vue 入口 JS
├── tests                    # 测试工具
├── README.md
└── package.json
```
## 本地开发 
```
$ yarn install
$ yarn run serve
```
## 路由配置
### Api
{ Route } 
<table>
  <tr>
    <th>参数</th>
    <th>说明</th>
    <th>类型</th>
    <th>默认值</th>
  </tr>
  <tr>
    <td>component</td>
    <td>组件 按异步引入组件<br>
    component: () => import('..')</td>
    <td>TSX</td>
    <td>-</td>
  </tr>
  <tr>
    <td>redirect</td>
    <td>重定向地址, 访问这个路由时,自定进行重定向<br>
    name||path</td>
    <td>string</td>
    <td>-</td>
  </tr>
   <tr>
    <td>name</td>
    <td>路由名称, 必须设置,且不能重名 <br>meta不存在将设置这个name为菜单名称	<br></td>
    <td>string</td>
    <td>-</td>
  </tr>
</table>

### meta
<table>
  <tr>
    <th>参数</th>
    <th>说明</th>
    <th>类型</th>
    <th>默认值</th>
  </tr>
  <tr>
    <td>title</td>
    <td>标题用于显示页面标题</td>
    <td>TSX</td>
    <td>-</td>
  </tr>
  <tr>
    <td>hidden</td>
    <td>是否显示在菜单中</td>
    <td>Boolean</td>
    <td>false</td>
  </tr>
   <tr>
    <td>name</td>
    <td>菜单名称	<br></td>
    <td>string</td>
    <td>-</td>
  </tr>
  <tr>
    <td>component</td>
    <td>标题插槽组件<br></td>
    <td>template</td>
    <td>-</td>
  </tr>
    <tr>
    <td>noCache</td>
    <td>是否不缓存<br></td>
    <td>Boolean</td>
    <td>false</td>
  </tr>
  <tr>
    <td>icon</td>
    <td>图标<br></td>
    <td>template</td>
    <td>-</td>
  </tr>
  <tr>
    <td>disabled</td>
    <td>禁用菜单<br></td>
    <td>Boolean</td>
    <td>-</td>
  </tr>
</table>

## Vuex结构
