import axios, { AxiosInstance } from 'axios'
import { message } from 'ant-design-vue'

const http: AxiosInstance = axios.create({
    /** 指定请求地址 */
    baseURL: process.env.VUE_APP_API
})
/** 请求拦截器 */
http.interceptors.request.use(function (config) {
    return config;
}, function (error) {
    return Promise.reject(error);
})
/** 响应拦截器 */
http.interceptors.response.use(function (config) {
    return config
}, function (error) {
    if (error.response.status === 401) {
        localStorage.clear()
        message.warn('验证错误,请重新登陆...')
        setTimeout(() => {
            location.href = '/login'
        }, 500);
    }
    return Promise.reject(error);
}
)
export default http