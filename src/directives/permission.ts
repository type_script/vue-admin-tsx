import { Directive } from 'vue'
import store from '@/store/modules/user'
const permission: Directive = {
    mounted(el, { value }) {
        const state: any = store.state
        if (value.indexOf(state.userinfo.role) === -1) {
            el.remove()
        }
    }
}
export default permission