const { mock } = require('mockjs')
const BASEURL = process.env.VUE_APP_API
const login = require('./mockapi/login')
const theme = require('./mockapi/theme')
// const table = require('./api/table')
const api = {
    ...login,
    ...theme,
    // ...table
}
mock(BASEURL + '/login', "post", (config) => {
    const data = api['login'](config)
    return data
})
mock(BASEURL + '/info', "post", (config) => {
    const data = api['info'](config)
    return data
})
mock(BASEURL + '/theme', "get", (config) => {
    const data = api['theme'](config)
    return data
})