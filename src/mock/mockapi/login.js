module.exports = {
    'login': (ctx) => {
        const { username, password } = JSON.parse(ctx.body)
        console.log(ctx)
        /** 管理员 */
        if (username === 'admin' && password === '888888') {
            const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSkFDSyIsImF2YXRhciI6Imh0dHBzOi8vd3BpbWcud2FsbHN0Y24uY29tL2Y3Nzg3MzhjLWU0ZjgtNDg3MC1iNjM0LTU2NzAzYjRhY2FmZS5naWY_aW1hZ2VWaWV3Mi8xL3cvODAvaC84MCIsInBvd2VyIjoiQURNSU4iLCJpYXQiOjE2MDM1OTU0NjF9.shRRoAKDlPszRDRLyG1u8gLw07_EohqHag78wdi9xFs'
            return { code: 20000, data: { token } }
        }
        /** 用户 */
        else if (username === 'user' && password === '888888') {
            const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSkFDSyIsImF2YXRhciI6Imh0dHBzOi8vd3BpbWcud2FsbHN0Y24uY29tL2Y3Nzg3MzhjLWU0ZjgtNDg3MC1iNjM0LTU2NzAzYjRhY2FmZS5naWY_aW1hZ2VWaWV3Mi8xL3cvODAvaC84MCIsInBvd2VyIjoiVVNFUiIsImlhdCI6MTYwMzU5NTUwM30.3TT_IAhL77GizSL0P3zTLM9NJ1471Mr-ag7qS06JeAI'
            return { code: 20000, data: { token } }
        }
        /** 账号密码错误 */
        else {
            return { code: 40001, data: { msg: '账号/密码错误...' } }
        }
    },
    'info': (ctx) => {
        const admin = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSkFDSyIsImF2YXRhciI6Imh0dHBzOi8vd3BpbWcud2FsbHN0Y24uY29tL2Y3Nzg3MzhjLWU0ZjgtNDg3MC1iNjM0LTU2NzAzYjRhY2FmZS5naWY_aW1hZ2VWaWV3Mi8xL3cvODAvaC84MCIsInBvd2VyIjoiQURNSU4iLCJpYXQiOjE2MDM1OTU0NjF9.shRRoAKDlPszRDRLyG1u8gLw07_EohqHag78wdi9xFs'
        const user = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSkFDSyIsImF2YXRhciI6Imh0dHBzOi8vd3BpbWcud2FsbHN0Y24uY29tL2Y3Nzg3MzhjLWU0ZjgtNDg3MC1iNjM0LTU2NzAzYjRhY2FmZS5naWY_aW1hZ2VWaWV3Mi8xL3cvODAvaC84MCIsInBvd2VyIjoiVVNFUiIsImlhdCI6MTYwMzU5NTUwM30.3TT_IAhL77GizSL0P3zTLM9NJ1471Mr-ag7qS06JeAI'
        const { token } = JSON.parse(ctx.body)
        if (token === admin) {
            return { code: 20000, data: { name: 'JACK', avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80', role: 'ADMIN' } }
        }
        if (token === user) {
            return { code: 20000, data: { name: 'JACK', avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80', role: 'USER' } }
        }
        return { code: 40001, data: { msg: '验证失败...请重新登陆' } }
    }
}