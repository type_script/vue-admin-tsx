import { Form, Input, Button, message } from 'ant-design-vue'
import { defineComponent, reactive } from 'vue'
import { UserOutlined, LockOutlined } from '@ant-design/icons-vue'
import { useRouter } from 'vue-router'
import styles from 'vue3-styled-components'
import { LoginApi } from '@/api/login'/** 登陆接口 */
import { ILogin } from '@/types'/** 接口 */

const LoginStyle = styles.div`width: 100%vh;height: 100vh;background: #f0f2f5 url(https://preview.pro.antdv.com/assets/background.a568162c.svg) no-repeat 50%;background-size: 100%;position: relative;`
const LoginTitle = styles.h1`text-align: center;padding-top: 150px;color: #1890FF; `
const LoginForm = styles(Form)` margin:auto;max-width: 480px;padding: 15px!important;`

const Login = defineComponent({
    setup() {
        const { Item } = Form
        const { Password } = Input
        const router = useRouter()
        const login_list = reactive<ILogin>({ username: '', password: '', type: false })
        const add = async (): Promise<void> => {
            login_list.type = !login_list.type
            const { code, data } = await LoginApi(login_list)
            /** 成功登陆 */
            if (code === 20000) {
                localStorage.token = JSON.stringify(data.token)
                console.log(data)
                router.push('/')
            } else {
                /** 错误提示 */
                message.error(data.msg)
                login_list.username = ''
                login_list.password = ''
                login_list.type = false
            }
        }
        const keyup = (event: { keyCode: any }) => { if (event.keyCode === 13) add() }
        return () => (
            <LoginForm>
                <Item>
                    <Input
                        onKeydown={keyup}
                        size="large"
                        disabled={login_list.type}
                        v-model={[login_list.username, 'value']}
                        allow-clear
                        v-slots={{ prefix: () => <UserOutlined /> }}
                        placeholder="账号:admin">
                    </Input>
                </Item>
                <Item>
                    <Password
                        onKeydown={keyup}
                        size="large"
                        disabled={login_list.type}
                        v-model={[login_list.password, 'value']}
                        allow-clear v-slots={{ prefix: () => <LockOutlined /> }}
                        placeholder="密码:888888">
                    </Password>
                </Item>
                <Item>
                    <Button size="large" onClick={add} loading={login_list.type} type="primary" block>登陆</Button>
                </Item>
            </LoginForm>
        )
    }
})
export default defineComponent({
    setup() {
        return () => (
            <LoginStyle>
                <LoginTitle>系统登陆</LoginTitle>
                <Login></Login>
            </LoginStyle>
        )

    }
})