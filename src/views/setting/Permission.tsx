// import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons-vue'
import styled from 'vue3-styled-components'
import { computed, defineComponent, watchEffect } from 'vue'
import { Button, Divider, Table, Tag, Modal } from 'ant-design-vue'
import ATree from '@/components/Tree'
import { useStore } from 'vuex'
import { GlobalState } from '@/store'
import { useRouter } from 'vue-router'
import { permission } from '@/directives'
import { ColumnProps } from 'ant-design-vue/types/table/column'
import RolesData from './RolesData'
import useBoolean from '@/hooks/useBoolean'
const Styles = styled.div`background:white;padding:20px`
const columns: ColumnProps<unknown>[] | undefined = [
    {
        align: "center",
        title: '用户名',
        dataIndex: 'name',
        width: 250

    },
    {
        align: "center",
        title: '用户权限',
        dataIndex: 'keys',
        width: 250
    },
    {
        align: "center",
        title: '描述',
        dataIndex: 'context',
    },
    {
        align: "center",
        title: '操作',
        slots: { customRender: 'operation' },
        width: 350
    }
]
const AModal = styled(Modal)``

export default defineComponent({
    directives: {
        permission: permission
    },
    setup() {
        const $store = useStore<GlobalState>()
        /**当前权限 */
        const $router = useRouter()
        const role = computed(() => $store.getters.roles)
        const roleType = computed(() => role.value === 'ADMIN')
        const setRoleType = (roles: string) => () => {
            localStorage.token = JSON.stringify(
                roles === 'ADMIN' ?
                    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSkFDSyIsImF2YXRhciI6Imh0dHBzOi8vd3BpbWcud2FsbHN0Y24uY29tL2Y3Nzg3MzhjLWU0ZjgtNDg3MC1iNjM0LTU2NzAzYjRhY2FmZS5naWY_aW1hZ2VWaWV3Mi8xL3cvODAvaC84MCIsInBvd2VyIjoiQURNSU4iLCJpYXQiOjE2MDM1OTU0NjF9.shRRoAKDlPszRDRLyG1u8gLw07_EohqHag78wdi9xFs' :
                    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSkFDSyIsImF2YXRhciI6Imh0dHBzOi8vd3BpbWcud2FsbHN0Y24uY29tL2Y3Nzg3MzhjLWU0ZjgtNDg3MC1iNjM0LTU2NzAzYjRhY2FmZS5naWY_aW1hZ2VWaWV3Mi8xL3cvODAvaC84MCIsInBvd2VyIjoiVVNFUiIsImlhdCI6MTYwMzU5NTUwM30.3TT_IAhL77GizSL0P3zTLM9NJ1471Mr-ag7qS06JeAI'
            )
            $router.go(0)
        }
        const addRoutes = computed(() => $store.getters.addRoutes.children)
        const [setVisible, Visible] = useBoolean(false)
        const data = computed(() => RolesData)
        const slots = {
            operation: (props: any) => <div>
                <Button type="primary" onClick={() => setVisible()}>编辑</Button>
                <Button type="danger">删除</Button>

            </div>
        }
        return () => (
            <Styles>
                <div>
                    <h1>菜单页面权限</h1>
                    <h4>
                        <a>通过addRoutes控制路由和菜单显示</a>
                        <div>点击下方切换权限</div>
                    </h4>
                    <Button onClick={setRoleType('USER')} type={!roleType.value ? 'primary' : 'default'}>User</Button>
                    <Button onClick={setRoleType('ADMIN')} type={roleType.value ? 'primary' : 'default'}>Admin</Button>
                </div>
                <Divider />
                <div>
                    <h1>指令权限</h1>
                    <h4>
                        <a>通过权限指令控制组件内容渲染</a>
                        <div>点击下方切换权限</div>
                    </h4>
                    <Button onClick={setRoleType('USER')} type={!roleType.value ? 'primary' : 'default'}>User</Button>
                    <Button onClick={setRoleType('ADMIN')} type={roleType.value ? 'primary' : 'default'}>Admin</Button>
                    <div>
                        {roleType.value ? <Tag color="pink">ADMIN渲染</Tag> : <Tag color="purple">USER渲染</Tag>}
                        <span><a>判断符</a></span>
                        <div>
                            <Tag color="cyan" v-permission="['USER']">使用指令</Tag>
                            <span><a>v-permission="['USER']"</a></span>
                        </div>
                        <div>
                            <Tag color="cyan" v-permission="['USER','ADMIN']">使用指令</Tag>
                            <span><a>v-permission="['USER','ADMIN']"</a></span>
                        </div>
                    </div>
                </div>
                <Divider />
                <div>
                    <h1>角色权限分配</h1>
                    <Table v-slots={slots} columns={columns} data-source={data.value} rowKey={(record: { name: string }) => record.name} />
                    <AModal title="常用菜单设置" v-model={[Visible.value, "visible"]}>
                        <ATree addRoutes={addRoutes} ></ATree>
                    </AModal>
                </div>
            </Styles>
        )
    }
})
