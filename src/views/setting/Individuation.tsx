import styled from 'vue3-styled-components'
import { defineComponent, markRaw, ref } from 'vue'
import { CheckOutlined } from '@ant-design/icons-vue'
import { Card, Switch, Button } from 'ant-design-vue'
const Styles = styled.div`
color:red
`
const PageStyles = styled.div`display: flex;padding: 10px;max-width: 300px;justify-content: space-around;`
const PageElement = styled.div`position: relative;cursor: pointer;`
const PageIcon = styled.span`position: absolute;bottom: 9px;right: 10px;color: #1890ff;`
const Theme = styled.div`float: left;width: 20px;height: 20px;margin-right: 8px;color: #fff;font-weight: 700;text-align: center;border-radius: 2px;cursor: pointer;`
const Btn = styled.div`float:right;padding:15px`
export default defineComponent({
    setup() {
        /** pageStyle */
        const PageStyleList = markRaw(['https://gw.alipayobjects.com/zos/antfincdn/NQ%24zoisaD2/jpRkZQMyYRryryPNtyIC.svg', 'https://gw.alipayobjects.com/zos/antfincdn/XwFOFbLkSM/LCkqqYNmvBEbokSDscrm.svg'])
        const numStyle = ref(1)
        const changePageStyle = (index: number) => () => {
            numStyle.value = index
        }
        /** pageMode */
        const PageModeList = markRaw(['https://gw.alipayobjects.com/zos/antfincdn/XwFOFbLkSM/LCkqqYNmvBEbokSDscrm.svg', 'https://gw.alipayobjects.com/zos/antfincdn/URETY8%24STp/KDNDBbriJhLwuqMoxcAr.svg'])
        const numMode = ref(1)
        const changeModeStyle = (index: number) => () => {
            numMode.value = index
        }
        /** Theme */
        const colorList = markRaw(['rgb(24, 144, 255)', 'rgb(245, 34, 45)', 'rgb(250, 84, 28)', 'rgb(19, 194, 194)', 'rgb(114, 46, 209)'])
        return () => (
            <Styles>
                <div>
                    <Card>
                        <div>
                            <h3>Page style setting</h3>
                            <PageStyles>
                                {PageStyleList.map((item, index) => (
                                    <PageElement onClick={changePageStyle(index)}>
                                        <img src={item} alt={item} />
                                        {index === numStyle.value ? <PageIcon><CheckOutlined /></PageIcon> : null}
                                    </PageElement>
                                ))}
                            </PageStyles>
                        </div>
                        <div>
                            <h3>Page mode setting</h3>
                            <PageStyles>
                                {PageModeList.map((item, index) => (
                                    <PageElement onClick={changeModeStyle(index)}>
                                        <img src={item} alt={item} />
                                        {index === numMode.value ? <PageIcon><CheckOutlined /></PageIcon> : null}
                                    </PageElement>
                                ))}
                            </PageStyles>
                        </div>
                        <div>
                            <h3>Tags-View</h3>
                            <Switch checked-children="开" un-checked-children="关"></Switch>
                        </div>
                        <div>
                            <h3>Theme Color</h3>
                            <div>
                                {colorList.map(item => <Theme style={{ background: item }}></Theme>)}
                            </div>
                        </div>
                        <div style="clear: both;">
                            <h3>Tags-Menus</h3>
                            <Switch checked-children="开" un-checked-children="关"></Switch>
                        </div>
                        <Btn>
                        <Button>重制</Button>
                        <Button type="primary">保存</Button></Btn>
                    </Card>
                </div>
            </Styles>
        )
    }
})
