import { message } from 'ant-design-vue'
import { useRouter } from 'vue-router';

export default class SetMenus {
    commonMenus: any;
    commonName: any;
    route: any;
    constructor() {
        /** 所有主题 */
        this.commonMenus = this.getCommonElement('commonMenus')
        /** 应用名称 */
        this.commonName = this.getCommonElement('commonName')
        this.route = useRouter()
    }
    getMenus() {
        return { commonMenus: this.commonMenus, commonName: this.commonName, flag: Object.keys(this.commonMenus).length === 0 }
    }
    getCommonElement(type: string) {
        let data = ''
        try {
            data = localStorage[type] ? JSON.parse(localStorage[type]) : ''

        } catch (error) {
            data = ''
        }
        return data
    }
    /**读取 */
    get() {
        let data: Array<any> = []
        try {
            const keys = Object.keys(this.commonMenus)
            keys.forEach(item => data.push({ name: item, args: this.commonMenus[item] }))
        } catch (error) {
            console.log(error)
            localStorage.commonMenus = ''
        }
        return data
    }
    /**设置 */
    set(value: string, AllCheck: Array<any>) {
        try {
            const data = this.commonMenus ? Object.assign(this.commonMenus, { [value]: AllCheck }) : { [value]: AllCheck }
            localStorage.commonMenus = JSON.stringify(data)
            localStorage.commonName = JSON.stringify(value)
        } catch (error) {
            localStorage.commonMenus = ''
            localStorage.commonName = ''
        }
    }
    /**应用 */
    apply(text: any) {
        localStorage.commonName = JSON.stringify(text.name ? text.name : '')
        message.success('菜单应用成功...')
        this.route.push({ name: '刷新中...' })
    }
    /**删除 */
    delete(text: any) {
        delete this.commonMenus[text.name]
        if (this.commonMenus === text.name) {
            localStorage.commonName = ''
        }
        localStorage.commonMenus = JSON.stringify(this.commonMenus)
        message.success('删除成功...')
        this.route.push({ name: '刷新中...' })
    }
    /**是否为主题 */
    disabled(name: string) {
        return this.commonName === name
    }
    /**初始化 */
    created() {
        if (Object.keys(this.commonMenus).length === 0) {
            localStorage.commonMenus = JSON.stringify({ 常规菜单01: ["00", "02", "3", "30", "31", "60"], 常规菜单02: ["00", "5", "50", "60"] })
            localStorage.commonName = JSON.stringify('常规菜单01')
            message.success('菜单初始化成功...')
            this.route.push({ name: '刷新中...' })
        }
    }
}