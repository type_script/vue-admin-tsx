// import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons-vue'
import styled from 'vue3-styled-components'
import { computed, defineComponent, nextTick, onMounted, reactive, ref } from 'vue'
import { Button, Input, message, Modal, Table } from 'ant-design-vue'
import { useStore } from 'vuex'
import ATree from '@/components/Tree'
import { ColumnProps } from 'ant-design-vue/types/table/column'
import useBoolean from '@/hooks/useBoolean'
import SetMenus from './SetMenus'
import router from '@/router'
const Styles = styled.div``
const AModel = styled(Modal)``
const AInput = styled.div`max-width: 300px;margin-bottom: 20px`
const Operation = styled.div`display: flex;justify-content: space-around`

const columns: ColumnProps<unknown>[] | undefined = [
    {
        align: "center",
        title: '菜单名称',
        dataIndex: 'name',
        slots: { customRender: 'name' },

    },
    {
        align: "center",
        title: '操作',
        slots: { customRender: 'tags' },
        width: 250
    }
]

export default defineComponent({
    setup() {
        const $store = useStore()
        const setMenus = new SetMenus()
        setMenus.created()
        const val = ref('')
        const historyMenus = ref<string[]>([])
        const [setVisible, Visible] = useBoolean(false)
        const [setLoading, Loading] = useBoolean(true)
        const [setValFlag, ValFlag] = useBoolean(false)
        const [setOk, Ok] = useBoolean(false)
        const addRoutes = computed(() => $store.getters.addRoutes.children)
        const apply = (props: any) => () => setMenus.apply(props.text)
        const del = (props: any) => () => setMenus.delete(props.text)
        const edit = (props: any) => () => {
            historyMenus.value = props.text.args
            setValFlag(true)
            setVisible(true)
            val.value = props.text.name
        }
        const slots = {
            tags: (props: any) => <Operation>
                <Button ghost type="primary" onClick={edit(props)}>
                    编辑
                </Button>
                <Button ghost type="primary" disabled={setMenus.disabled(props.text.name)} onClick={apply(props)}>
                    应用
                </Button>
                <Button ghost type="danger" onClick={del(props)}>
                    删除
                </Button>
            </Operation>,
            name: (props: any) => {
                return <a>{props.text}</a>
            }
        }
        const add = () => {
            if (val.value.length > 0) {
                historyMenus.value = []
                setOk(true)
                message.success('更新菜单成功...')
                router.go(0)

            } else {
                message.warn('请填写菜单名称...')
            }
            nextTick(() => {
                setOk(false)
                setVisible(false)
                val.value = ''
                setValFlag(false)
            })
        }
        /** 初始化表格 */
        const init = () => {
            data.push(...setMenus.get())
            setLoading()
        }
        const data = reactive<{ name: string, args: any }[]>([])
        onMounted(() => init())
        return () => (
            <Styles>
                <Button style="margin:10px" type="primary" size="large" onClick={() => setVisible()}>新建常用菜单设置</Button>
                <AModel title="常用菜单设置" v-model={[Visible.value, "visible"]} onOk={add} >
                    <AInput>
                        <span style="font-weight:bold">常用菜单名称</span>
                        <Input disabled={ValFlag.value} placeholder="输入常用名称..." v-model={[val.value, 'value']} />
                    </AInput>
                    <ATree addRoutes={addRoutes} Ok={Ok.value} historyMenus={historyMenus.value} val={val}></ATree>
                </AModel>
                <Table v-slots={slots} columns={columns} data-source={data} bordered loading={Loading.value} rowKey={(record: { name: number; }) => record.name} />
            </Styles>
        )
    }
})
