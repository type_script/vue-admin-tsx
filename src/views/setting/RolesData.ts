export default [
    {name:'Admin',keys:'ADMIN',context:'超级管理员。有权查看所有页面',subMenus:[]},
    {name:'普通用户',keys:'USER',context:'普通编辑器。可以看到除权限页面以外的所有页面',subMenus:[]},
    {name:'游客',keys:'VISITOR',context:'只是一个访客。只能看到主页和文档页面',subMenus:[]}
]