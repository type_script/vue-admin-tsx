import { TableApi } from '@/api/table'
import { Table, Tag, Form, Input, Button } from 'ant-design-vue'
import { defineComponent, onMounted, reactive, readonly, ref, watch } from 'vue'
import { EditOutlined } from '@ant-design/icons-vue'
// import '@/styles/table.css'

const TableName = defineComponent({
    props: ['text', 'index', 'inputState', 'onState'],
    setup(props: any) {
        const type = ref(false)
        const add = (e: any) => {
            type.value = !type.value
            props.onState()
        }
        watch(() => props.inputState.value, (val) => {
            console.log(val)

            if (!val) { type.value = false }

        })
        onMounted(() => input.value = props.text)
        const input = ref('')
        return () =>
            <div class="name-tablelist" onClick={e => e.stopPropagation()}>
                <Input style="width:130px" disabled={!type.value ? true : false} v-model={[input.value, 'value']}></Input>
                <EditOutlined class="edit-table" onClick={add} />
            </div>
    }
})

const TableTitle = () => {
    const { Item } = Form
    return (<>
        <Form layout='inline'>
            <Item label="Field A"><Input></Input></Item>
            <Item label="Field A"><Input></Input></Item>
            <Item><Button type="primary">查询</Button></Item>
            <Item><Button>重置</Button></Item>
        </Form>
        <Button type="primary">新建</Button>
    </>)
}
export default defineComponent({
    setup() {
        const inputState = ref(false)
        const ChangeState = () => { if (inputState.value) inputState.value = false }
        const onState = () => inputState.value = true
        const columns = [
            { dataIndex: 'index', title: '#' },
            { dataIndex: 'name', title: 'name', slots: { customRender: 'name' }, width: 190 },
            { dataIndex: 'date', title: 'date', width: 130 },
            { dataIndex: 'city', title: 'city', width: 150 },
            { dataIndex: 'tag', title: 'tag', slots: { customRender: 'tags' } },
            { dataIndex: 'content', title: 'content' },
        ]
        let data: any = reactive([])
        onMounted(async () => data.push(...await TableApi()))
        const slots = {
            tags: ({ text }: any) => <Tag color={text[0]}>{text[0].slice(1)}</Tag>,
            name: (props: any) => <TableName {...props} inputState={inputState} onState={onState} />,
            title: () => <TableTitle />
        }
        return () => (
            <div onClick={ChangeState}><Table style="padding:15px" columns={columns} data-source={data} v-slots={slots} /></div>
        )
    }
})