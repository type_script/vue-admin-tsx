// import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons-vue'
import styled from 'vue3-styled-components'
import { defineComponent, onMounted } from 'vue'
import { RouterView } from 'vue-router'
import Sortable from "sortablejs"
import { Table, Tag } from 'ant-design-vue'
import data from './data'
const Styles = styled.div`
color:red
`
const columns: any = [
    {
        align: 'center',
        title: '#',
        slots: { customRender: 'index' },
        width:50
    },
    {
        align: 'align',
        title: 'name',
        dataIndex: 'name',
        key: 'name',
        slots: { title: 'customTitle', customRender: 'name' },
    },
    {
        align: 'align',
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
    },
    {
        align: 'align',
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
    },
    {
        align: 'align',
        title: 'Tags',
        dataIndex: 'tags',
        slots: { customRender: 'tags' },
    },
]
export default defineComponent({
    setup() {
        const slots = {
            index: ({ index }: any) => <a>{index}</a>,
            tags: ({ text }: any) => <>{text.map((item: any) => <Tag color="pink">{item}</Tag>)}</>

        }
        onMounted(() => {
            var el: HTMLElement | null = document.querySelector('.ant-table-tbody')
            Sortable.create(el!, {
                ghostClass: 'sortable-ghost',
                animation: 150,
            });
        })

        return () => (
            <Styles>
                <Table
                    size="middle"
                    v-slots={slots}
                    bordered={true}
                    tableLayout='fixed'
                    id="items"
                    columns={columns}
                    data-source={data}></Table>
            </Styles>
        )
    }
})
