import { defineComponent, nextTick, onMounted, reactive, watch } from 'vue'
import styled from 'vue3-styled-components'
// 引入 ECharts 主模块
var echarts = require('echarts/lib/echarts');
// // 引入柱状图
require('echarts/lib/chart/line');
require('echarts/theme/macarons') // echarts theme
const Styles = styled.div`background: white;margin:auto;max-width:1200px;padding:10px`
import SearchLinedata from './SearchLinedata'
export default defineComponent({
    props: ['LineData'],
    setup(props) {
        nextTick(() => {
            const myChart = echarts.init(document.getElementById('SearchLine'), 'macarons');
            myChart.setOption(SearchLinedata)
            window.addEventListener('resize', () => myChart.resize())
        })
        return () => (
            <Styles>
                <div id="SearchLine" style="height:250px;width:300px"></div>
            </Styles>
        )
    }
})