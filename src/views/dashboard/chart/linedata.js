export default {
    xAxis: {
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        boundaryGap: false,
        axisTick: {
            show: false
        }
    },
    grid: {
        left: 10,
        right: 10,
        bottom: 20,
        top: 30,
        containLabel: true
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross'
        },
        padding: [5, 10]
    },
    yAxis: {
        axisTick: {
            show: false
        }
    },
    legend: {
        data: ['expected', 'actual']
    },
    series: [{
        name: 'expected', itemStyle: {
            normal: {
                color: '#ffb980',
                lineStyle: {
                    color: '#ffb980',
                    width: 2
                }
            }
        },
        smooth: true,
        type: 'line',
        data: [100, 120, 161, 134, 105, 160, 165],
        animationDuration: 2800,
        animationEasing: 'cubicInOut'
    },
    {
        name: 'actual',
        smooth: true,
        type: 'line',
        itemStyle: {
            normal: {
                color: '#2ec7c9',
                lineStyle: {
                    color: '#2ec7c9',
                    width: 2
                },
                areaStyle: {
                    color: '#b2ebeb'
                }
            }
        },
        data: [120, 82, 91, 154, 162, 140, 145],
        animationDuration: 2800,
        animationEasing: 'quadraticOut'
    }]
}