import { defineComponent, nextTick, onMounted, reactive, watch } from 'vue'
import styled from 'vue3-styled-components'
// 引入 ECharts 主模块
var echarts = require('echarts/lib/echarts');
// // 引入柱状图
require('echarts/lib/chart/line');
require('echarts/theme/macarons') // echarts theme
// require('echarts/theme/tech-blue') // echarts theme

const Styles = styled.div`background: white;margin:auto;max-width:1200px;padding:10px`
import linedata from './linedata'
export default defineComponent({
    props: ['LineData'],
    setup(props) {
        let UpdateChart: any = null
        watch(() => props.LineData.value, (val) => {
            UpdateChart(JSON.parse(val))
        })
        nextTick(() => {
            const myChart = echarts.init(document.getElementById('c2'), 'macarons');
            myChart.setOption(linedata)
            window.addEventListener('resize', () => myChart.resize())
            UpdateChart = (data: any) => {
                var option = myChart.getOption()
                option.series[0].data = data.expectedData
                option.series[1].data = data.actualData
                myChart.setOption(option)
            }
        })
        return () => (
            <Styles>
                <div id="c2" style="height:320px;"></div>
            </Styles>
        )
    }
})