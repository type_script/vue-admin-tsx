export default { tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b}: {c} ({d}%)'
},
legend: {
    left: 0,
    data: ['直接访问', '邮件营销', '联盟广告', '视频广告', '搜索引擎']
},
series: [
    {
        name: '访问来源',
        type: 'pie',
        radius: ['40%', '65%'],
        avoidLabelOverlap: false,
        label: {
            show: false,
            position: 'center'
        },
        emphasis: {
            label: {
                show: true,
                fontSize: '20',
                fontWeight: 'bold'
            }
        },
        labelLine: {
            show: false
        },
        data: [
            {value: 1335, name: '直接访问'},
            {value: 310, name: '邮件营销'},
            {value: 534, name: '联盟广告'},
            {value: 735, name: '视频广告'},
            {value: 348, name: '搜索引擎'}
        ]
    }
]}