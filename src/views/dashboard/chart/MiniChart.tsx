import { defineComponent, nextTick } from 'vue'
import styled from 'vue3-styled-components'
/** ECharts */
const echarts = require('echarts/lib/echarts');
require('echarts/theme/macarons') /** echarts theme */

const Styles = styled.div`height:280px;width:100%;display:flex;justify-content: center;`
export default defineComponent({
    props: ['type'],
    setup(props) {
        nextTick(async () => {
            const path = props.type
            await import("echarts/lib/chart/" + path) /** echarts */
            const Options = (await import('./' + path + 'data.js')).default /** 数据 */

            /** 基于准备好的dom，初始化echarts实例 */
            const myChart = echarts.init(document.getElementById(path), 'macarons')
            /** 绘制图表 */
            myChart.setOption(Options)
            /** 响应式处理 */
            window.onresize = () => myChart.resize()

        })
        return () => (
            <Styles id={props.type} ></Styles>
        )
    }
})