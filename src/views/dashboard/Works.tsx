// import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons-vue'
import styled from 'vue3-styled-components'
import { defineComponent} from 'vue'
import { Card, Avatar,List } from 'ant-design-vue'
const Styles = styled.div`
color:red
`
export default defineComponent({
    setup() {
        const GridList =[
            { name: 'React', src: "https://react-1251415695.cos-website.ap-chengdu.myqcloud.com/favicon-32x32.png?v=f4d46f030265b4c48a05c999b8d93791", content: '...' },
            { name: 'Vue', src: "https://v3.cn.vuejs.org/logo.png", content: '...' },
            { name: 'Angular', src: "https://angular.cn/assets/images/logos/angular/shield-large.svg", content: '...' },
            { name: 'RxJs', src: "https://cn.rx.js.org/manual/asset/Rx_Logo_S.png", content: '...' },
            { name: 'NestJs', src: "https://d33wubrfki0l68.cloudfront.net/e937e774cbbe23635999615ad5d7732decad182a/26072/logo-small.ede75a6b.svg", content: '...' }
        ]
        const { Grid } = Card
        return () => (
            <Styles>
                <Card title="项目进行中...">
                    {GridList.map(item => <Grid style="width:33.33%;text-align:center">
                        <Avatar src={item.src}></Avatar>
                        <h3>{item.name}</h3>
                        {item.content} 12小时前
                    </Grid>)}
                </Card>
                <Card title="动态">
                    <List>
                        
                    </List>
                </Card>
            </Styles>
        )
    }
})
