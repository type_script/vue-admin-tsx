import { UsergroupAddOutlined, MessageOutlined, ShoppingCartOutlined, MoneyCollectOutlined } from '@ant-design/icons-vue'
import styled from 'vue3-styled-components'
import { defineComponent, markRaw, reactive, ref } from 'vue'
import Line from './chart/Line'
import MiniChart from './chart/MiniChart'
import PersonalNote from '@/components/PersonalNote'
import SearchMessage from './SearchMessage'
import { Card } from 'ant-design-vue'
import '@/styles/dashboard.css'

const AHeader = styled.div`width:97%;margin:auto;background: white;margin-top:10px`
const ACard = styled.div`display: flex;justify-content: space-around;margin: 25px 0px;flex-wrap: wrap;`
const CardItem = styled.div`display: flex;align-items: center;justify-content: space-between;`
const CardIcon = styled('span', { Color: String })`color:${(props: { Color: String }) => props.Color || "#1890ff"};font-weight: bold;font-size: 40px;padding:5px 15px;transition: 1s all ease;&:hover {background: ${(props: { Color: any }) => props.Color || "#1890ff"};border-radius: 5px;color: white;}`
const CardDescribe = styled.span``
const CardTitle = styled.p`font-weight: bold;color: rgba(0,0,0,.45);`
const CardNum = styled('p', { Color: String })`font-size: 18px;font-weight: bold;&:hover {color:${(props: { Color: String }) => props.Color || "#1890ff"}}`
const ChartElement = styled.div`display: flex;justify-content: space-around;flex-wrap:wrap;overflow: hidden;`
const ChartItem = styled.div`background: white;padding: 2px;width:30%;margin-top:30px;border-radius: 10px;`
const AFooter = styled.div`display: flex;justify-content: space-around;flex-wrap:wrap;margin-top:20px`
export default defineComponent({
    setup() {
        /** 渲染数据 */
        const PanelList: any[] = [
            { title: 'New Visits', num: '10,2400', icon: <UsergroupAddOutlined />, color: '#40c9c6' },
            { title: 'Messages', num: '10,9923', icon: <MessageOutlined />, color: '#1890ff' },
            { title: 'Shoppings', num: 996, icon: <ShoppingCartOutlined />, color: '#f4516c' },
            { title: 'Total amount', num: '900,2400', icon: <MoneyCollectOutlined />, color: '#ffb980' }
        ]
        const updateLine = (index: any) => () => LineData.value = JSON.stringify(lineChartData[index])
        let LineData = ref('')
        const lineChartData = [
            { expectedData: [100, 120, 161, 134, 105, 160, 165], actualData: [120, 82, 91, 154, 162, 140, 145] },
            { expectedData: [200, 192, 120, 144, 160, 130, 140], actualData: [180, 160, 151, 106, 145, 150, 130] },
            { expectedData: [80, 100, 121, 104, 105, 90, 100], actualData: [120, 90, 100, 138, 142, 130, 130] },
            { expectedData: [130, 140, 141, 142, 145, 150, 160], actualData: [120, 82, 91, 154, 162, 140, 130] }
        ]
        const MiniChartList = ['pie', 'radar', 'bar']
        return () => (
            <div>
                <AHeader>
                    <Line LineData={LineData} />
                </AHeader>
                <ACard>
                    {PanelList.map((item, index) =>
                        <Card hoverable class='card-dashboard' bodyStyle={{ padding: '20px 13px' }} onClick={updateLine(index)}>
                            <CardItem>
                                <CardIcon Color={item.color}>{item.icon}</CardIcon>
                                <CardDescribe class='describe-dashboard'>
                                    <CardTitle>{item.title}</CardTitle>
                                    <CardNum Color={item.color}>{item.num}</CardNum>
                                </CardDescribe>
                            </CardItem>
                        </Card>)}
                </ACard>
                <ChartElement>
                    {MiniChartList.map(item => <ChartItem class='minchart--dashboard'><MiniChart type={item}></MiniChart></ChartItem>)}
                </ChartElement>
                <AFooter>
                    <SearchMessage class="SearchMessage-dashboard"></SearchMessage>
                    <PersonalNote class="PersonalNote-dashboard" ></PersonalNote>
                </AFooter>
            </div>
        )
    }
})
