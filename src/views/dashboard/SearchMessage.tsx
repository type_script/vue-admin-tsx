// import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons-vue'
import { defineComponent } from 'vue'
import styled from 'vue3-styled-components'
import { Card, Statistic, Table } from 'ant-design-vue'
import SearchLine from './chart/SearchLine'
const AHeader = styled.div`display:flex;align-items: center;`
export default defineComponent({
  setup() {
    const columns = [
      {
        title: '排名',
        dataIndex: 'name',
        sorter: true,
        width: '20%',
        slots: { customRender: 'name' },
      },
      {
        title: '搜索关键词',
        dataIndex: 'name',
        sorter: true,
        width: '30%',
        slots: { customRender: 'name' },
      },
      {
        title: '来源',
        dataIndex: 'gender',
        filters: [
          { text: 'Male', value: 'male' },
          { text: 'Female', value: 'female' },
        ],
        width: '20%',
      },
      {
        title: '地区',
        dataIndex: 'email',
        width: '20%',
      },
    ];
    return () => (
      <Card title="搜索信息">
        <div>
          <AHeader>
            <div>
              <Statistic title="搜索用户数" value="112893"></Statistic>
              <Statistic title="人均搜索次数" value="3.2"></Statistic>
            </div>
            {/* <SearchLine></SearchLine> */}
          </AHeader>
          <div></div>
        </div>
        <Table columns={columns}></Table>
      </Card>

    )
  }
})
