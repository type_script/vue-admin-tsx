import { Layout, Drawer } from 'ant-design-vue'
import {
    defineComponent, markRaw
} from 'vue'
import { RouterView, onBeforeRouteUpdate, useRoute } from 'vue-router'
const { Header, Content, Footer } = Layout
import styled from 'vue3-styled-components'

import '@/styles/home.css'
/** 封装Layout */
import LayoutMenu from '@/layout/LayoutMenu'
import LayoutHeader from '@/layout/LayoutHeader'
import LayoutCard from '@/layout/LayoutCard'
/** hook */
import useBoolean from '@/hooks/useBoolean'

const ALayout = styled(Layout)`min-height:100vh`
const AHeader = styled(Header)`position:fixed; z-index: 10; width: 100vw;box-shadow:0 1px 4px rgba(0,21,41,.08);padding:0`
const AContent = styled(Content, { loading: Boolean })`margin:20px;transition: 200ms all ease-in-out 0s;opacity:${(props: { loading: string }) => props.loading ? .1 : 1};`
const ALayoutCard = styled(LayoutCard)`margin-top:64px`
const ALayoutMenu = styled(LayoutMenu)`position:fixed; z-index: 10;right: 0;top: 220px;border-radius: 10px 0px 0px 10px;opacity: .8;width: 60px;`

export default defineComponent({
    setup() {
        const $route = useRoute()
        const [setVisible, Visible] = useBoolean(false)
        const [setLoading, Loading] = useBoolean(false)
        const DrawerAttrs = {
            drawerStyle: { background: '#001529' },//'#001529'
            bodyStyle: { padding: 0 },
            headerStyle: { background: '#001529', color: 'wheat', border: 0 },
        }
        /** 路由跳转副作用 */
        onBeforeRouteUpdate(() => {
            setVisible(false)
            setLoading(true, () => setTimeout(() => { setLoading(false) }, 500))
        })
        return () => (
            <ALayout>
                <AHeader>
                    <Drawer {...DrawerAttrs} placement="left" v-model={[Visible.value, 'visible']} v-slots={{ title: () => <span style="color:wheat">Vue Admin Tsx</span> }}>
                        <LayoutMenu mode="Drawer" visible={Visible.value} />
                    </Drawer>
                    <LayoutHeader v-model={[Visible.value, 'visible']} />
                </AHeader>
                <ALayoutCard v-slots={{ content: () => $route.meta.template ? <$route.meta.template /> : null }} />
                <AContent loading={Loading.value}>
                    <RouterView />
                </AContent>
                <Footer>Vue3 Admin Tsx ©2020 </Footer>
                <ALayoutMenu collapsed={true} mode="Common"   ></ALayoutMenu >
            </ALayout>
        )
    }
})
