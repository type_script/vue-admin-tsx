import styled from 'vue3-styled-components'
import { defineComponent, nextTick, onMounted, ref } from 'vue'
import { Input } from 'ant-design-vue'
const Styles = styled.div`width:95vw;height:70vh;position: relative;margin:auto;`
const MapStyle = styled.div`width:95vw;height:70vh;`
const AInput = styled(Input)`position: absolute;z-index: 20;max-width: 200px;left: 20px;top:10px`
import { mapInit, mapSearch } from './AMapLoader'
export default defineComponent({
    setup() {
        let map = null
        onMounted(() => {
            nextTick(async () => {
                const AMap = await mapInit(['AMap.ToolBar'])
                map = new AMap.Map('container', {
                    mapStyle: 'amap://styles/macaron',
                    resizeEnable: true
                })
                map.addControl(new AMap.ToolBar())
                mapSearch(map,'input',AMap,'广州')
            })
        })
        return () => (
            <Styles>
                <AInput placeholder="关键词搜索" id="input" />
                <MapStyle id="container"></MapStyle>
            </Styles>
        )
    }
})
