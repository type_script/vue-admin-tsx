import AMapLoader from '@amap/amap-jsapi-loader'
/**初始化地图函数 */
export function mapInit(plugins:Array<string>) {
    return AMapLoader.load({
        "key": "2bb391c340d3044bc9754fd6f6ddb14e",              // Key
        version: '2.0',
        plugins: ['AMap.PlaceSearch', 'AMap.AutoComplete',...plugins],
    })
}
/**关键词搜索函数 */
export function mapSearch<T>(map: T, node: string, AMap: { AutoComplete: new (arg0: { input: string; city: string; }) => any; PlaceSearch: new (arg0: { map: T; city: string; }) => any; }, city: string): void {
    const auto = new AMap.AutoComplete(
        {
            input: node,
            city
        }
    )
    const placeSearch = new AMap.PlaceSearch({
        map: map,
        city
    })
    auto.on("select", select);//注册监听，当选中某条记录时会触发
    function select(e: { poi: { adcode: any; name: any; }; }) {
        placeSearch.setCity(e.poi.adcode);
        placeSearch.search(e.poi.name);  //关键字查询查询
    }

}
