import { defineComponent, reactive, markRaw, readonly, ref } from 'vue'
import { Tabs, Card, Empty, Form, Input, DatePicker, Cascader, Upload, Button, List } from 'ant-design-vue'
import styled from 'vue3-styled-components'
const AvatarImg = styled.img`width: 125px;height: 125px;border-radius: 50%`
const AForm = styled(Form)`max-width: 500px`
/** 个人设置 */
const Base = defineComponent({
    setup() {
        const { Item } = Form
        const { TextArea } = Input
        /** 个人设置 */
        const InputList = markRaw([
            { name: '昵称', placeholder: '请填写自己的名称', type: Input, value: 'name' },
            { name: '个性签名', placeholder: 'Your are not alone.', type: TextArea, value: 'bio' },
            { name: '生日', placeholder: '2020/02/29', type: DatePicker, value: 'birthday' },
            { name: '电子邮件', placeholder: 'admin@test.com', type: Input, value: 'email' },
            { name: '所属部门', placeholder: 'select', type: Cascader, value: 'department' },
            { name: '密码', placeholder: '请填写自己的密码', type: Input, value: 'bio' }
        ])
        /** v-model */
        const form: any = reactive({
            name: '',
            bio: '',
            password: '',
            email: '',
            birthday: '',
            department: []
        })
        /** 头像上传地址 */
        const imageUrl = ref('https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif')
        return () => <div>
            <h1>个人设置</h1>
            <AvatarImg src={imageUrl.value} class='head-setting' alt="avatar" />
            <AForm layout="vertical" model={form} class='form-setting'>
                {InputList.map(i => <Item key={i.name} label={i.name}>
                    <i.type placeholder={i.placeholder} v-model={[form[i.value], 'value']}></i.type>
                </Item>)}
            </AForm>
            <Upload>
            </Upload>
            <Button type="primary">提交</Button>
            <Button >保存</Button>
        </div>
    }
})
/** 安全设置 */
const Security = defineComponent({
    setup() {
        const { Item } = List
        const { Meta } = Item
        const list = markRaw([
            { title: '账户密码', content: '当前密码强度 : 强' },
            { title: '密保手机', content: '已绑定手机 : 10086' },
            { title: '密保问题', content: '未设置密保问题，密保问题可有效保护账户安全' },
            { title: '邮箱设置', content: '未邮箱...' }

        ])
        return () =>
            <>
                <h1>安全设置</h1>
                <List>
                    {list.map(item => <Item v-slots={{ actions: () => <><a>修改</a></> }} >
                        <Meta title={item.title} description={item.content}></Meta>
                    </Item>
                    )}
                </List>
            </>
    }
})
/** 个性化 */
const Personalize = defineComponent({
    setup() {
        const { Item } = List
        const { Meta } = Item
        const list = markRaw([
            { title: '风格设置', content: '整体风格配色设置' },
            { title: '主题色', content: '页面风格配色： 默认' },
        ])
        return () =>
            <>
                <h1>个性化</h1>
                <List>
                    {list.map(item => <Item v-slots={{ actions: () => <><a>修改</a></> }} >
                        <Meta title={item.title} description={item.content}></Meta>
                    </Item>
                    )}
                </List>
            </>
    }
})
export default defineComponent({
    setup() {
        const { TabPane } = Tabs
        const TabList = readonly([
            { name: '个人设置', template: <Base /> },
            { name: '安全设置', template: <Security /> },
            { name: '个性化', template: <Personalize /> },
            { name: '消息通知' },
            { name: '时间线' },
            { name: '活动' }
        ])
        return () => <>
            <Card>
                <Tabs tabPosition="left">
                    {TabList.map((i) => {
                        return <TabPane key={i.name} tab={i.name}>{i.template ||
                            <div>
                                <h1>{i.name}</h1>
                                <Empty description={false}></Empty>
                            </div>
                        }</TabPane>
                    })}
                </Tabs>
            </Card></>
    }
})
