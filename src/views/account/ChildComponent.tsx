import { defineComponent, onMounted, reactive } from 'vue'
import { Timeline, Card, Comment, Tooltip, Avatar, Input, Button } from 'ant-design-vue'
import { ClockCircleOutlined, LikeFilled, DislikeFilled } from '@ant-design/icons-vue';
import { ThemeApi } from '@/api/theme-data';

export const Dynamic = defineComponent({
    setup() {
        const { TextArea } = Input
        const slots = {
            author: () => <a>Han Solo</a>,
            content: () => <p>hello world</p>,
            datetime: () => <Tooltip><span>2020-09-18 09:15:40</span></Tooltip>,
            actions: () => <><LikeFilled></LikeFilled><span>299</span><DislikeFilled></DislikeFilled><span>10</span></>,
            avatar: () => <Avatar src="https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80" alt="Han"></Avatar>

        }
        return () =>
            <>
                <div style="display:flex">
                    <Avatar  style="margin-right:10px"  src="https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80" alt="Han"></Avatar>
                    <TextArea></TextArea>
                </div>
                <Button style="margin-left: 40px;margin-top: 20px;" type="primary">Add Comment</Button>
                {new Array(10).fill(1).map((item, index) => <Comment key={index.toString()} v-slots={slots}></Comment>)}
            </>
    }
})
export const TimeAxis = defineComponent({
    setup() {
        const { Item } = Timeline
        return () =>
            <>
                <h1>时间线</h1>
                <Timeline style="margin-top:70px" mode="alternate">
                    <Item>Create a services site 2020-09-01</Item>
                    <Item color="green">Solve initial network problems 2020-09-01</Item>
                    <Item v-slots={{ dot: () => <ClockCircleOutlined style="font-size: 16px;" /> }}>
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                        laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
                        beatae vitae dicta sunt explicabo.
                </Item>
                    <Item color="red">Network problems being solved 2020-09-01</Item>
                    <Item color="blue">Create a services site 2020-09-02</Item>
                    <Item>Technical testing 2020-09-01</Item>
                </Timeline>
            </>
    }
})
export const Project = defineComponent({
    setup() {
        let themedata: any = reactive([])
        onMounted(async () => {
            const res = await ThemeApi()
            themedata.push(...res)
        })
        return () =>
            <>
                {themedata.map((item: any) => <Card style="max-width:400px;margin:auto" v-slots={{ cover: () => <img src={item.image} /> }} >{item.name}</Card>)}
            </>
    }
})