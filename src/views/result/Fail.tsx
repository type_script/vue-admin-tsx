// import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons-vue'
import { defineComponent, onMounted, ref } from 'vue'
import { RouteConfig_d } from '@/interface'
import { Result, Button } from 'ant-design-vue'
import { useRoute } from 'vue-router'

export const Component = defineComponent({
    setup() {
        const $route = useRoute()
        const status = ref<"success" | "error" | "info" | "warning" | "404" | "403" | "500" | undefined>('success')
        const title = ref('Successfully Purchased Cloud Server ECS!')
        const subTitle = ref("Order number: 2017182818828182881 Cloud server configuration takes 1-5 minutes, please wait.")
        if ($route.path === '/result/fail') {
            status.value = "error"
            title.value = "Successfully Purchased Cloud Server ECS!"
            subTitle.value = "There are some problems with your operation."
        }
        return () => (
            <>
                <Result status={status.value} title={title.value}
                    sub-title={subTitle.value}
                    extra={() => <><Button type="primary">Go Console</Button><Button >Buy Again</Button></>}
                >

                </Result>
            </>
        )
    }
})
export const RouteConfig: RouteConfig_d = { meta: {}, name: '失败', path: 'fail' }