// import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons-vue'
import styled from 'vue3-styled-components'
import { defineComponent } from 'vue'
import { RouteConfig_d } from '@/interface'
import { Result,Button } from 'ant-design-vue'

export const Component = defineComponent({
    setup() {
        return () => (
            <>
            <Result status="success" title="Successfully Purchased Cloud Server ECS!"
            sub-title="Order number: 2017182818828182881 Cloud server configuration takes 1-5 minutes, please wait."
            extra={()=><><Button type="primary">Go Console</Button><Button >Buy Again</Button></>}
            >

            </Result>
            </>
        )
    }
})
export const RouteConfig: RouteConfig_d = { meta: {}, name: '成功', path: 'success' }