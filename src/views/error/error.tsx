import { LeftOutlined } from '@ant-design/icons-vue'
import styled from 'vue3-styled-components'
import { computed, defineComponent } from 'vue'
import { useRoute } from 'vue-router'
import Notfound from './NotFound'
import ServerReporting from './ServerReporting'
import ValidationFailed from './ValidationFailed'
import { Button } from "ant-design-vue"
const Styles = styled.div`display: flex;width: 80%;height: 70vh;padding-top: 12vh;`
const ErrorTitle = styled.div`  padding-top: 30px;`
export default defineComponent({
    setup() {
        const $route = useRoute()
        const store: any = computed(() => {
            return error[$route.path] ?error[$route.path]:error['/error/404']
        })
        const error: any = {
            '/error/404': { path: '404', title: '404 notfound', content: 'Sorry, the page you visited does not exist.', components: Notfound },
            '/error/500': { path: '500', title: '500 ValidationFailed', content: 'Sorry, the server is reporting an error.', components: ServerReporting },
            '/error/401': { path: '401', title: '401 ValidationFailed', content: 'Sorry, You don\'t have permission to go to the page.', components: ValidationFailed }

        }
        return () => (
            <Styles>
                <store.value.components></store.value.components>
                <ErrorTitle>
                    <h1>{store.value.title}</h1>
                    <h3>{store.value.content}</h3>
                    <a href="/">
                        <Button type="primary"> <LeftOutlined />Backward </Button>
                    </a>
                </ErrorTitle>
            </Styles>
        )
    }
})
