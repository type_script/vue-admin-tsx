import { RouteRecordRaw, RouterView } from 'vue-router'
const AsyncRegister = async (fn: any) => ({ component: async () => (await fn).Component, ...await (await (fn)).RouteConfig })
import { UserOutlined, SettingOutlined, BugOutlined,ToolOutlined } from '@ant-design/icons-vue'
import { markRaw } from 'vue'
const basic: RouteRecordRaw[] = [
    {
        path: '/SupplierAllBack',
        name: '刷新中...',
        meta:{hidden:true},
        component: () => import('@/layout/SupplierAllBack')
      },
    {
        path: '/plugin', name: '插件页', meta: { name: '插件页', icon: ToolOutlined  }, component: markRaw(RouterView),
        children: [
            {meta:{},name:'地图页',path:'map',component:()=>import('@/views/plugins/Map')}
        ]
    },
    {
        path: '/account', name: '个人页', meta: { name: '个人页', icon: UserOutlined }, component: markRaw(RouterView),
        children: [
            { meta: {}, name: '个人中心', path: 'center', component: () => import('@/views/account/Center') },
            { meta: {}, name: '个人设置', path: 'setting', component: () => import('@/views/account/Setting') }
        ]
    },
    {
        path: '/error', name: '错误页', meta: { name: '错误页', icon: BugOutlined }, component: markRaw(RouterView),
        children: [
            { meta: {}, name: '404', path: '404', component: () => import('@/views/error/error') },
            { meta: {}, name: '500', path: '500', component: () => import('@/views/error/error') },
            { meta: {}, name: '401', path: '401', component: () => import('@/views/error/error') }
        ]
    },
    {
        path: '/setup', name: '设置页', meta: { name: '设置页', icon: SettingOutlined }, component: markRaw(RouterView),
        children: [
            { meta: {}, name: '常用菜单', path: 'menu', component: () => import('@/views/setting/CommonMenu') },
            { meta: {}, name: '用户权限', path: 'perm', component: () => import('@/views/setting/Permission') },
            { meta: {}, name: '个性化', path: 'individuation', component: () => import('@/views/setting/Individuation') }
        ]
    }
]
export default basic