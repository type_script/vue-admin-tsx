import { RouteRecordRaw, RouterView } from 'vue-router'
import { markRaw } from 'vue'
import { DashboardOutlined, CheckCircleOutlined, BorderOuterOutlined, TableOutlined,FormOutlined } from '@ant-design/icons-vue'
import UserCard from '@/components/UserCard'
// const AsyncRegister = async (fn: any) => ({ component: async () => (await fn).Component, ...await (await (fn)).RouteConfig })

const other: RouteRecordRaw[] = [
    {
        path: '/dashboard', meta: { name: '仪表盘', icon: DashboardOutlined },
        component: markRaw(RouterView),
        name: '仪表盘',
        redirect: { name: '分析页' },
        children: [
            { meta: { name: '分析页', title: '分析页-首页', content: '分析页面 用于展示可视化数据' }, name: '分析页', path: 'homepage', component: () => import('@/views/dashboard/HomePage') },
            { meta: { name: '监控页', title: '监控页-首页', disabled: true }, name: '监控页', path: 'monitor', component: markRaw(RouterView) },
            { meta: { name: '工作台', title: '工作台-首页', content: '工作台页面 个人工作台展示项目详细', template: markRaw(UserCard) }, name: '工作台', path: 'works', component: () => import('@/views/dashboard/Works') }

        ]
    },
    {
        path: '/route', name: '嵌套路由', meta: { name: '嵌套路由', icon: BorderOuterOutlined }, component: markRaw(RouterView),
        children: [
            {
                path: 'route1', name: '嵌套路由1', meta: { name: '嵌套路由1' }, component: markRaw(RouterView),
                children: [
                    { path: 'route2', name: '嵌套路由2', meta: { name: '嵌套路由2' }, component: markRaw(RouterView) },
                    { path: 'route3', name: '嵌套路由3', meta: { name: '嵌套路由4' }, component: markRaw(RouterView) }]
            }]
    },
    {
        path: '/table', name: '表格页', meta: { name: '表格页', icon: TableOutlined }, component: markRaw(RouterView),
        children: [
            { path: 'drag-table', name: '拖拽Tabs', meta: { name: '拖拽Tabs' }, component: () => import('@/views/tables/DraggableTable') },
            { path: 'table', name: '动态Tabs', meta: { name: '动态Tabs' }, component: () => import('@/views/tables/TableList') }]

    },
    {
        path: '/result', name: '结果页', meta: { name: '结果页', icon: CheckCircleOutlined }, component: markRaw(RouterView),
        children: [
            { meta: {}, name: '成功', path: 'success', component: () => import('@/views/result/index') },
            { meta: {}, name: '失败', path: 'fail', component: () => import('@/views/result/index') }
        ]
    },
    {
        path:'/forms',name:'表单页面',
        meta:{icon:FormOutlined},
        component: markRaw(RouterView),
    }


]
export default other