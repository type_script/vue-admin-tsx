import other from './other'
import basic from './basic'
import { RouteRecordRaw } from 'vue-router'
/** meta 
 * power:string null 权限 Admin | User
 * icon:component null 菜单图标 
 * noCache:boolean false 缓存
 * title:string null 页面名称
 * hidden:boolean true 是否显示在菜单
 * name:string route.name 菜单名称 
 * disabled:boolean false 禁用
 * 
**/
const routes: RouteRecordRaw = {
    /** 后台页面 
     * await AsyncRegister(import('@/views/Demo')) => {path:xxx,component,...}(这种方式也是支持的) || 
     * add children {...await AsyncRegister(import('@/views/Demo')),children:[...]}
    **/
    meta: {}, name: '首页', path: '/home', redirect: '/dashboard/homepage',
    component: () => import('@/views/Home'),
    children: [
        /** 其他路由渲染 */
        ...other,
        /** 基本路由渲染 */
        ...basic
    ]
}
export default routes