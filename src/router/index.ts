import { createRouter, createWebHistory, RouteRecordRaw, createWebHashHistory } from 'vue-router'
/** 
 * 默认渲染 login页面 其他通过addRoute渲染
**/
const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'home',
    redirect: () => localStorage.token ? '/home' : '/login'
  },
  {
    path: '/login',
    component: () => import('@/views/Login'),
    name: '登陆页面',
    meta: {
      noCache: true
    },
  },
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
