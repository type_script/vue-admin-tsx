// import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons-vue'
import styled from 'vue3-styled-components'
import { defineComponent } from 'vue'
import { Avatar, Badge, Statistic } from 'ant-design-vue'
const Styles = styled.div``
const Content = styled.div`display: flex;align-items: center;`
const AStatistic = styled.div`display: flex;`
export default defineComponent({
    setup() {
        return () => (
            <Styles>
                <Badge count="99">
                    <Avatar size={64} src="https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif"></Avatar>
                </Badge>
                <Content>
                    <h1>Good afternoon. Feel the fish</h1>
                    <span style="margin-left:20px">前端攻城师 | 技术部- VUE3-TSX组</span>
                </Content>
                <AStatistic>
                    <Statistic title="项目数" value="1024"></Statistic>
                    <Statistic style="padding-left: 10px;" title="项目排名" value="10/24"></Statistic>
                    <Statistic style="padding-left: 10px;" title="项目访问" value="996"></Statistic>
                </AStatistic>
            </Styles>
        )
    }
})
