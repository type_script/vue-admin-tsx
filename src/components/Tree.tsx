import styled from 'vue3-styled-components'
import { Tree } from 'ant-design-vue'
import { computed, defineComponent, onMounted, ref, watch, watchEffect } from 'vue'
import SetMenus from '@/views/setting/SetMenus'


const ATree = styled(Tree)``
export default defineComponent({
    /** 路由信息 是否点击确定 input值 */
    props: ['addRoutes', 'Ok', 'val', 'historyMenus'],
    setup(props: any) {
        const addRoutes = computed(() => props.addRoutes.value)
        const val = computed(() => props.val.value)
        const checkedKeys = ref<any>([])
        const setMenus = new SetMenus()
        onMounted(() => {
            if (props.historyMenus&&props.historyMenus.length > 0) {
                checkedKeys.value.push(...props.historyMenus.map((item: string) => '0-' + item.split('').join('-')))
            }
        })
        watch(() => props.Ok, (OkValue) => {
            if (OkValue) {
                const AllCheck = checkedKeys.value.map((item: any) => item.replace(/-/g, '').slice(1)).sort()
                setMenus.set(val.value, AllCheck)
            }
        })
        watch(() => props.historyMenus, (val) => {
            checkedKeys.value.push(...props.historyMenus.map((item: string) => '0-' + item.split('').join('-')))
        })
        return () => <ATree
            checkable={true}
            replaceFields={{ children: 'children', title: 'name' }}
            v-model={[checkedKeys.value, 'checkedKeys']}
            tree-data={addRoutes.value}>
        </ATree >
    }
})