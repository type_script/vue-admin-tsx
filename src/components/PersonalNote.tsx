import { defineComponent, reactive, readonly } from "vue"
import { SettingOutlined, EditOutlined, EllipsisOutlined, SendOutlined, ClusterOutlined } from '@ant-design/icons-vue';
import { Card, Tag, Divider, Progress } from 'ant-design-vue'
import '@/styles/center.css'

/** 个人介绍 */
const PersonalNote = defineComponent({
    setup() {
        const slots = {
            actions: () => {
                return (<>
                    <SettingOutlined></SettingOutlined>
                    <EditOutlined></EditOutlined>
                    <EllipsisOutlined></EllipsisOutlined>
                </>)
            }
        }
        const tags = reactive(['Javascript', 'Node', 'Vue', 'Typescript', 'Mongoose', 'React', 'Sass'])
        const colors = readonly(['pink', 'red', 'orange', 'green', 'cyan', 'blue', 'purple'])
        const list = readonly([{ percent: 30, name: '项目1', status: 'exception' }, { percent: 70, name: '项目2', status: 'active' }, { percent: 50, name: '项目3', status: 'active' }, { percent: 100, name: '项目4', status: 'success' }])

        return () =>
            <Card title="关于我" v-slots={slots} class="center-PersonalNote">
                <img src="https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif" alt="avatar" class="center-avatar" />
                <div>
                    <h1>JACK</h1>
                    <div><ClusterOutlined /><span class='center-text'>前端开发工程师</span></div>
                    <div><SendOutlined /><span class='center-text'>广东省广州市猎德</span></div>
                </div>
                <Divider />
                <div>
                    <h4>标签</h4>
                    <div>
                        {tags.map((item, index) => <Tag key={item} color={colors[index]}>{item}</Tag>)}
                    </div>
                </div>
                <Divider />
                <div>
                    <h4>项目进度</h4>
                    {list.map((item: { status: any; name: string | number | undefined; percent: number | undefined; }) => <>
                        <h4>{item.name}</h4>
                        <Progress strokeWidth={15} status={item.status} key={item.name} percent={item.percent}></Progress></>)}
                </div>
            </Card>
    }
})
export default PersonalNote