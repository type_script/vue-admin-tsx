import { Module } from 'vuex'
import { GlobalState } from '..'
import { IPermission } from '@/types'
import addRoutes from '@/router/modules'
const store: Module<IPermission, GlobalState> = {
    state: {
        addRoutes: []
    },
    mutations: {
        /** 赋值动态路由 */
        RoutesApi(context, data) { context.addRoutes = data }
        /** filter */
    },
    actions: {
        /** 获取动态路由Api接口 */
        async RoutesApi(context): Promise<void> {
            /** 后端渲染路由 */
            /** 走本地路由 */
            context.commit('RoutesApi', addRoutes)
        }
    },
    getters: {
        /** 获取用户权限 */
        addRoutes: ({ addRoutes }) => addRoutes,
        // /** 获取token */
        // token: () => localStorage.token
    }
}

export default store
