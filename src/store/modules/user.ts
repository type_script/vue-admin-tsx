import { InfoApi } from '@/api/login'
import { Module } from 'vuex'
import { GlobalState } from '..'
import { IUserState } from '@/types'

const store: Module<IUserState, GlobalState> = {
    state: {
        userinfo: null,
    },
    mutations: {
        /** 给userinfo赋值 */
        InfoApi(context, data) { context.userinfo = data }
    },
    actions: {
        /** 获取用户Api接口 */
        async InfoApi(context): Promise<void> {
            const { data } = await InfoApi({ token: JSON.parse(localStorage.token) })
            // console.log(data)
            context.commit('InfoApi', data)
        }
    },
    getters: {
        /** 获取用户权限 */
        roles: ({ userinfo }) => userinfo?.role,
        /** 获取token */
        token: () => JSON.parse(localStorage.token),
        avatar: ({ userinfo }) => userinfo?userinfo.avatar:''
    }
}

export default store
