import { IRoutingHistory } from '@/types'
import { Module } from 'vuex'
import { GlobalState } from '..'


const store: Module<IRoutingHistory, GlobalState> = {
    state: {
        RoutingHistory: ['个人中心']
    },
    mutations: {
        updateHistory: ({ RoutingHistory }, { name, meta }) => {
            if (meta.hidden) return
            if (name && !RoutingHistory.includes(name)) RoutingHistory.push(name)
            if (RoutingHistory.length > 6) RoutingHistory.shift()
        },
        closeHistory: ({ RoutingHistory }, name) => {
            if (RoutingHistory.length > 1) {
                let index = RoutingHistory.indexOf(name)
                RoutingHistory.splice(index, 1)
            }

        }
    },
    actions: {

    },
    getters: {
    }
}

export default store
