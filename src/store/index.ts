import { createStore, StoreOptions } from 'vuex'
// import app from './modules/app'
import user from './modules/user'
import permission from './modules/permission'
import router from './modules/router'
import {IPermission, IRoutingHistory, IUserState,} from '@/types'
export interface GlobalState {
  permission:IPermission,
  user:IUserState
  router:IRoutingHistory
}
export default createStore<GlobalState>({
  modules: {
    // app,
    user, 
    permission,
    router
  }
})
