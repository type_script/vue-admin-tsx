// import {
//     Transition,
//     Component as DynamicComponent,
//     resolveDynamicComponent,
//     defineComponent,
//     defineAsyncComponent
// } from 'vue'

import { defineComponent } from 'vue'
import { RouterView } from 'vue-router'
export default defineComponent({
    setup() {
        return () => (
            <RouterView/>

        )
    }
})
