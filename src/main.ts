import { createApp } from 'vue'
import 'ant-design-vue/dist/antd.css'
import './styles/index.css'
import App from './App'
import store from './store'
import router from './router'
import 'default-passive-events'
//首次进入loading
import { message } from 'ant-design-vue'
message.loading('页面在努力加载中...', 1)
//mock数据
import './mock/mock'
//路由拦截和设置
import './router/permission'

const app = createApp(App)
app
    .use(store)
    .use(router)
    .mount('#app')
// app.config.errorHandler = (err:any, vm, info) => {
//     // let error = { info, url: location.href, msg: err }
//     if (localStorage.error) {
//         let error = JSON.parse(localStorage.error)
//         error.push({ info, url: location.href, msg: err.valueOf() })
//         localStorage.error = JSON.stringify(error)
//     } else {
//         localStorage.error = JSON.stringify([{ info, url: location.href, msg: err }])
//     }
//     // 处理错误
//     // `info` 是 Vue 特定的错误信息，比如错误所在的生命周期钩子
// }
export default app
