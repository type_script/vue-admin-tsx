import { RouteRecordRaw } from 'vue-router';
interface ILogin {
    username: string,
    password: string,
    type?: boolean
}
interface IPermission {
    addRoutes: RouteRecordRaw[] | []
}
interface IUserState {
    userinfo: undefined|null | { role: string,avatar:string }
}
interface IRoutingHistory {
    RoutingHistory: Array<string>
}
export {
    ILogin,
    IPermission,
    IUserState,
    IRoutingHistory
}