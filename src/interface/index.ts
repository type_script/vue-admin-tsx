export interface RouteConfig_d {
    meta?: any,
    name: string,
    path: string,
    redirect?: any
}