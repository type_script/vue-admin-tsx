import http from '../plugins/http'
/** 默认表格Api */
export async function TableApi() {
    const { data } = await http.get('tablelist')
    return data
}