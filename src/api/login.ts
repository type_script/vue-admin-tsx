import http from '../plugins/http'
import { ILogin } from '@/types'/** 接口 */

/** 登陆Api */
export async function LoginApi(params: ILogin) {
    const { data } = await http.post('login', params)
    return data
}
/** 查询信息Api */
export async function InfoApi(params: { token: string }) {
    const { data } = await http.post('info', params)
    return data
}
/** 退出Api */
export async function LogoutApi(params: { token: string }) {
    const { data } = await http.post('info', params)
    return data
}