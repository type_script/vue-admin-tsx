import http from '../plugins/http'
/** 获取项目Api */
export async function ThemeApi() {
    const { data } = await http.get('theme')
    return data
}