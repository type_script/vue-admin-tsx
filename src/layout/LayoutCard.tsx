import { HomeOutlined } from '@ant-design/icons-vue'
import styled from 'vue3-styled-components'
import { computed, defineComponent, ref, renderSlot } from 'vue'
import { Collapse, PageHeader, Breadcrumb, Tabs } from 'ant-design-vue'
import { useRoute, useRouter } from 'vue-router'
import { useStore } from 'vuex'
const { Panel } = Collapse
const { Item: BreadcrumbItem } = Breadcrumb
const { TabPane } = Tabs
const Styles = styled(Collapse)`background: white;padding:10px 15px 0px;`
const CollapsePanel = styled(Panel)`background: white;border-radius: 4px;border: 0;overflow: hidden`
const ExtraStyles = styled.div`max-width:600px`
export default defineComponent({
    setup(props, { slots }) {
        const $router = useRouter()
        const $route = useRoute()
        const $store = useStore()
        const activeKey = ref(['1'])
        const name: any = computed(() => $route.name)
        const Jump = (name: any) => $router.push({ name: name })
        /** 路由历史 */
        const RoutingHistory = computed(() => $store.state.router.RoutingHistory)
        /** 面包屑&组件 */
        const breadcrumbList: any =computed(() =>$route.path.split('/').filter(Boolean))
        /** 面包屑&组件 */
        const Header = () => (
            <div onClick={e => e.stopPropagation()}>
                <Breadcrumb>
                    <BreadcrumbItem><HomeOutlined /></BreadcrumbItem>
                    <BreadcrumbItem>首页</BreadcrumbItem>
                    {breadcrumbList.value.map((item: string | undefined) => <BreadcrumbItem key={item} href={item}>{item}</BreadcrumbItem>)}
                </Breadcrumb>
            </div>)
        /** 选项tabs */
        const Extra = () =>
            <ExtraStyles class="Home-Extra" onClick={(e: any) => { e.stopPropagation() }}>
                <Tabs tabBarStyle={{ padding: 0, margin: 0 }} onChange={Jump} activeKey={name.value}>
                    {RoutingHistory.value.map((item: any) => <TabPane key={item} tab={item}></TabPane>)}
                </Tabs>
            </ExtraStyles>

        return () => (
            <Styles bordered={false} expandIcon={Header} v-model={[activeKey.value, 'activeKey']}>
                <CollapsePanel key='1' extra={Extra}>
                    <PageHeader
                        title={$route.meta.title || $route.name}
                        sub-title={$route.meta.content}
                        onBack={() => $router.go(-1)}
                    ></PageHeader>
                    {renderSlot(slots, 'content')}
                </CollapsePanel>
            </Styles>
        )
    }
})
