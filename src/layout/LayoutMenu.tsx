import { Menu } from "ant-design-vue"
import { RouteRecordRaw, useRoute, useRouter } from 'vue-router'
import { useStore } from 'vuex'
import { computed, defineComponent, reactive } from 'vue'
import useInitMenu from '@/hooks/useInitMenu'
import SetMenus from '@/views/setting/SetMenus'
const useInit = (config: RouteRecordRaw[]) => {
    const $router = useRouter()
    const { Item, SubMenu } = Menu
    const Jump = (name: string | symbol | undefined, path?: any) => () => name ? $router.push({ name: name }) : window.open('_blank')!.location = path
    return config.map(item => {
        if (!item || item.meta && item.meta.hidden) return null
        const { meta }: any = item
        let slots = null
        /* title */
        if (item.children) slots = {
            title: () => <span>{meta && meta.icon ? <meta.icon /> : null}<span>{meta && meta!.name ? meta!.name : item.name}</span></span>
        }
        return <>{
            item.children && item.children.length > 0 ?
                <SubMenu v-slots={slots} key={item.path[0] === '/' ? item.path : '/' + item.path} disabled={meta && meta.disabled}>
                    {useInit(item.children)}
                </SubMenu>
                :
                <Item key={item.path} onClick={Jump(item.name, item.path)} disabled={meta && meta.disabled}>
                    <span>
                        {meta && meta.icon ? <meta.icon /> : null}
                        <span>
                            {meta && meta!.name ? meta!.name : item.name}
                        </span>
                    </span>
                </Item>
        }</>

    })
}

export default defineComponent({
    props: {
        mode: String,
        visible: Boolean,
        collapsed: Boolean
    },
    setup(props) {
        /** 用于判断类型 */
        const LayoutMode = computed(() => props.mode)
        const collapsed = computed(() => props.collapsed)
        const $store = useStore()
        const $route = useRoute()
        /** 菜单按需求渲染 */
        const addRoutes = computed(() => {
            if (LayoutMode.value === 'Drawer') return $store.getters.addRoutes.children
            if (LayoutMode.value === 'Common') {
                const setMenus = new SetMenus()
                const { flag, commonMenus, commonName } = setMenus.getMenus()
                return flag ?
                    useInitMenu([], $store.getters.addRoutes.children) :
                    useInitMenu(commonMenus[commonName], $store.getters.addRoutes.children)
            }
        })
        const listKeys = reactive<{
            openKeys: string[];
            selectedKeys: never | string[];
        }>({
            openKeys: [],
            selectedKeys: []
        })
        /** 渲染菜单 */
        const Rendering = () => {
            const keys = $route.path.split('/').filter(Boolean)
            listKeys.selectedKeys = [keys.length === 1 ? ('/' + keys[0]) : keys[keys.length - 1]]
            if (keys.length > 1) listKeys.openKeys = keys.map(i => '/' + i)
        }
        if (LayoutMode.value === 'Drawer') Rendering()
        /** 后续改成v-models */
        const add = (e: { key: any; }) => { listKeys.selectedKeys = [e.key] }
        return () => (
            <Menu inline-collapsed={collapsed.value} selectable={true} theme="dark" mode='inline' v-model={[listKeys.openKeys, 'openKeys']} selectedKeys={listKeys.selectedKeys} onClick={add}>
                {useInit(addRoutes.value)}
            </ Menu>
        )

    }
})
