import { Dropdown, Menu, Avatar, message } from 'ant-design-vue'
import { MenuUnfoldOutlined, RedoOutlined, CloseOutlined } from '@ant-design/icons-vue'
import { computed, defineComponent } from 'vue'
import { SettingOutlined, UserOutlined, ToolOutlined, PoweroffOutlined, SearchOutlined, FullscreenOutlined, GlobalOutlined } from '@ant-design/icons-vue'
import { useStore } from 'vuex'
import styled from 'vue3-styled-components'
import { useRoute, useRouter } from 'vue-router'
import useEmit from '@/hooks/useEmit'
import { GlobalState } from '@/store'

const DropdownMenu = () => {
    const $router = useRouter()
    const { Item, Divider } = Menu

    const logout = async () => {
        /** 清除token api 后端返回Api */
        // await LogoutApi({token:localStorage.token})
        localStorage.token = ''
        $router.push({ name: 'home' })
    }
    return (
        <Menu>
            <Item onClick={() => $router.push({ name: '个人中心' })}><UserOutlined />个人中心</Item >
            <Item onClick={() => $router.push({ name: '个人设置' })}><SettingOutlined />个人设置</Item>
            <Item disabled><ToolOutlined />权限设置</Item>
            <Divider></Divider>
            <Item onClick={logout}><PoweroffOutlined />退出登录</Item>
        </Menu>
    )
}


export default defineComponent({
    props: {
        visible: Boolean
    },
    setup(props, { emit }) {
        /** 组件 */
        const Styles = styled.div`margin-right:10px;display:flex;align-items:center`
        const ATrigger = styled.span`color: wheat;height: 64px;padding: 19px 15px;font-size: 20px;cursor: pointer;line-height: 32px;&:hover{background:#00000087}`
        /**store route */
        const $store = useStore<GlobalState>()
        const $router = useRouter()
        const $route = useRoute()
        const RoutingHistory = computed(() => $store.state.router.RoutingHistory)
        const index = computed(() => RoutingHistory.value.length)

        /** 菜单展示 */
        const [setVisible,] = useEmit(props, emit, 'visible')
        /** 头像下拉菜单插槽 */
        const slots = {
            overlay: () => <DropdownMenu />
        }
        /** 头像src */
        const avatar = computed(() => $store.getters.avatar)
        /** 刷新函数 */
        const Refresh = () => $router.push({ name: '刷新中...' })
        const Close = () => {
            $store.commit('closeHistory', $route.name)
            $router.push(index.value === 1 ? { name: RoutingHistory.value[0] } : { name: RoutingHistory.value[index.value - 1] })
            message.loading(index.value === 1 ? '这是我的底线了...' : '页面关闭成功...', .5)
        }
        return () => (
            <Styles>
                {/* 菜单 刷新 关闭 icon */}
                <ATrigger onClick={setVisible}><MenuUnfoldOutlined /></ATrigger>
                <ATrigger onClick={Refresh}><RedoOutlined /></ATrigger>
                <ATrigger onClick={Close}><CloseOutlined /></ATrigger>
                <div style="flex:1">
                    <div class="Home-Logo">Vue Admin Tsx</div>
                </div>
                {/* <ATrigger onClick={Close}><FullscreenOutlined/></ATrigger> */}
                <ATrigger onClick={Close}><SearchOutlined /></ATrigger>
                <div>
                    <Dropdown v-slots={slots}>
                        <div class="ant-pro-global-header-trigger">
                            <Avatar src={avatar.value} shape="square" size="large" onClick={e => e.preventDefault()}> </Avatar>
                        </div>
                    </Dropdown>
                </div>
                <ATrigger><GlobalOutlined /></ATrigger>
            </Styles>
        )
    }
})