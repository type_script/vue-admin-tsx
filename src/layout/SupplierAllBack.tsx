import styled from 'vue3-styled-components'
import { defineComponent } from 'vue'
import { useRouter } from 'vue-router'
export default defineComponent({
    setup() {
        const router = useRouter()
        router.go(-1)
        return () => (<div></div>)
    }
})
