import { ref, Ref } from 'vue'

export default function (boolean: Boolean) {
    const val: Ref<Boolean> = ref<Boolean>(boolean)
    const setVal: any = (boolean = !val.value, callback?: Function) => {
        val.value = boolean
        if (callback) callback()
    }
    return [setVal, val]
}