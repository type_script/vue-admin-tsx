export default function useEmit(props: any, emit: any, name: string):[() => any, any] {
    return [() => emit('update:visible', !props[name]), props[name]]
}