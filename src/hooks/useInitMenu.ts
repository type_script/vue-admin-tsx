export default function initMenu(AllCheck: any[], addRoutes: any[]) {
    let oldRoutes = addRoutes
    const index = AllCheck.length
    const menus: any[] = []
    let setMenus = menus
    let routes = oldRoutes
    let i = 0
    while (i < index) {
        const lastItem = AllCheck[i + 1]
        const oldItem = AllCheck[i - 1]
        const lastItemLength = lastItem && lastItem.length
        const Item = AllCheck[i]
        const ItemLength = Item.length
        /** 首次渲染子路由 */
        if (i === 0 && ItemLength > 1) {
            const Active = Item.split('')
            const PopItem = Active.pop()
            Active.forEach((j: any) => {
                setMenus[j] = { ...routes[j] }
                setMenus[j].children = []
                setMenus = setMenus[j].children
                routes = routes[j].children
            })
            setMenus[PopItem] = { ...routes[PopItem] }
            setMenus[PopItem].children = []
            i++
            continue
        }
        /** 包括父路由 */
        if (ItemLength === 1) {
            setMenus = menus
            routes = addRoutes
            setMenus[Item] = { ...routes[Item] }
            setMenus[Item].children = []
            routes = routes[Item].children
            setMenus = setMenus[Item].children
            i++
            continue
        }
        /** 上级路由不对等 */
        if (oldItem && oldItem[0] !== Item[0]) {
            setMenus = menus
            routes = addRoutes
            const Active = Item.split('')
            const PopItem = Active.pop()
            Active.forEach((j: any) => {
                setMenus[j] = { ...routes[j] }
                setMenus[j].children = []
                setMenus = setMenus[j].children
                routes = routes[j].children
            })
            setMenus[PopItem] = { ...routes[PopItem] }
            setMenus[PopItem].children = []
            i++
            continue
        }
        /** 结束 */
        if (!lastItemLength || lastItemLength === 1 || Item[0] !== lastItem[0]) {
            setMenus[Item.slice(-1)] = { ...routes[Item.slice(-1)] }
            setMenus[Item.slice(-1)].children = []
            setMenus = menus
            routes = addRoutes
            i++
            continue
        }
        /** 普通 */
        if (lastItemLength === ItemLength && Item[0] === lastItem[0]) {
            setMenus[Item.slice(-1)] = { ...routes[Item.slice(-1)] }
            setMenus[Item.slice(-1)].children = []
            i++
            continue
        }
        i++
    }
    return menus
}