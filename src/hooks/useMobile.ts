import { ref } from 'vue'
/** 处理Boolean的ref */
export default function () {
    const Mobile = ref(document.body.clientWidth > 1000 ? false : true)
    window.onresize = () => {
        const W: number = document.body.clientWidth
        W > 1000 ? Mobile.value = false : Mobile.value = true
    }
    return Mobile
}