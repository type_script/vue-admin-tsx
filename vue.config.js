// const isProd = process.env.NODE_ENV === 'production'
// const assetsCDN = {
//   // webpack build externals
//   externals: {
//     vue: 'Vue',
//     // 'vue-router': 'VueRouter',
//     // vuex: 'Vuex',
//     // axios: 'axios'
//   },
//   css: [],
//   // https://unpkg.com/browse/vue@2.6.10/
//   js: [
//     '//cdn.jsdelivr.net/npm/vue@3.0.0/dist/vue.global.prod.js',
//     // '//cdn.jsdelivr.net/npm/vue-router@3.1.3/dist/vue-router.min.js',
//     // '//cdn.jsdelivr.net/npm/vuex@3.1.1/dist/vuex.min.js',
//     // '//cdn.jsdelivr.net/npm/axios@0.19.0/dist/axios.min.js'
//   ]
// }

module.exports = {
  // devServer: {
  //   proxy: 'http://localhost:3000'
  // },
  publicPath: process.env.NODE_ENV === 'production'
    ? '/vue-admin-tsx/'
    : '/',
  productionSourceMap: false,
 
}